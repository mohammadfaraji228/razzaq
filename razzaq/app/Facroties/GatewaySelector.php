<?php

namespace App\Facroties;

use App\Interfaces\Gateways\GatewayInterface;
use App\Models\Transaction;
use App\Services\Gateway\JibitGateway;

class GatewaySelector
{
    private static $gatewayList = [
        'JIBIT' => ['class' => JibitGateway::class],
//        'VANDAR' => ['class' => VandarGateway::class],
    ];

    public static function selectGatewayAndPay(Transaction $transaction)
    {
        /**
         * @var GatewayInterface $gateway
         */
        foreach (static::$gatewayList as $gatewayObj => $class)
        {
            $gateway = new $class();
            $pay = $gateway->pay($transaction);
            if (key_exists('url',$pay))
            {
                return $pay;
            }
        }
        return false;
    }

    public static function getGateway(Transaction $transaction)
    {
        $class = static::$gatewayList[$transaction->gateway->name]['class'];

        return new $class($transaction);
    }

}
