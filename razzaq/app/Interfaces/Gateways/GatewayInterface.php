<?php

namespace App\Interfaces\Gateways;

use App\Models\Transaction;
use Illuminate\Http\Request;

interface GatewayInterface
{
    /**
     * @param Transaction $transaction
     */
    public function pay(Transaction $transaction);

    /**
     * @param Transaction $transaction
     * @param Request $request
     */
    public function callback(Transaction $transaction, Request $request);

    /**
     * @param Transaction $transaction
     */
    public function verify(Transaction $transaction);


    /**
     * @param Transaction $transaction
     */
    public function getTransactionData(Transaction $transaction);


}
