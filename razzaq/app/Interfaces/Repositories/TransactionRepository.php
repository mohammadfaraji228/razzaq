<?php

namespace App\Interfaces\Repositories;

interface TransactionRepository
{
    /**
     * @param $data
     * @return array
     */
    public function createTransaction($data): array;

    public function findTransactionByToken($token): string;

    public function validateTransactionStatus($transaction, $status);

    public function changeTransactionStatus($transaction, $status);
}
