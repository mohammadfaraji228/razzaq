<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $data
 * @property string $name
 * @property string $description
 */
class gateway extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'data',
        'description',
    ];
}
