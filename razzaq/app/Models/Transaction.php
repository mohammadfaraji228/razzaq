<?php

namespace App\Models;

use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Util\Json;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $gateway_id
 * @property string $token
 * @property Date $token_expires_in
 * @property integer $amount
 * @property string $status
 * @property string $gateway_token
 * @property string $factor_number
 * @property string $card_number_mask
 * @property string $gateway_tx_ref
 * @property json $extra_data
 * @property Gateway $gateway
 */
class Transaction extends Model
{
    const CREATED_STATUS = 'CREATED';
    const EXPIRED_STATUS = 'EXPIRED';
    const SENT_BANK_STATUS = 'SENT_BANK';
    const WAIT_FOR_CLIENT_VERIFICATION_STATUS = 'WAIT_FOR_CLIENT_VERIFICATION';
    const VERIFIED_STATUS = 'VERIFIED';
    const RETURNED_FROM_BANK = 'RETURNED_FROM_BANK';

    const SUCCEED = "SUCCEED";
    const FAILED = "FAILED";

    protected $fillable = [
        'id',
        'user_id',
        'gateway_id',
        'token',
        'token_expires_in',
        'amount',
        'status',
        'gateway_token',
        'factor_number',
        'card_number_mask',
        'gateway_tx_ref',
        'extra_data',
    ];


    public function gateway()
    {
        return $this->belongsTo(Gateway::class, 'gateway_id');
    }
}
