<?php

namespace App\Http\Controllers;

use App\Facroties\GatewaySelector;
use App\Interfaces\Repositories\TransactionRepository;
use App\Models\Transaction;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use \Illuminate\Http\JsonResponse;

class PaymentController extends Controller
{

    private $transactionService;


    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionService = $transactionRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getToken(Request $request): JsonResponse
    {
        $validate = Validator::make($request->all(),
            [
                'user_id' => 'required',
                'amount' => 'required|numeric',
                'factor_number' => 'required',
                'token_expires_in' => 'required',
            ]);

        if ($validate->failed()) {
            return response()->json(['message' => 'request not valid'], 403);
        }

        $transactionData = $this->transactionService->createTransaction($request->all());

        return response()->json($transactionData);
    }

    public function sendGateway($token)
    {
        /**
         * @var Transaction $transaction
         */
        $transaction = $this->transactionService->findTransactionByToken($token);

        $validate = $this->transactionService->validateTransactionStatus($transaction, Transaction::CREATED_STATUS);

        if (!$validate) {
            return response()->json(['error' => 'transaction expired'], 406);
        }
        $pay = gatewaySelector::selectGatewayAndPay($transaction);

        if ($pay == false) {
            return response()->json('error occurred', 500);
        }

        return view('gateways.gateway', $pay);
    }


    public function callback(Request $request, Transaction $transaction)
    {

        try {
            $this->transactionService->validateTransactionStatus($transaction, Transaction::SENT_BANK_STATUS);

            $transaction = $this->transactionService->changeTransactionStatus($transaction, Transaction::RETURNED_FROM_BANK);

            $gateway = GatewaySelector::getGateway($transaction);
            $call_back = $gateway->callback($transaction, $request);

            $transaction = $this->transactionService->changeTransactionStatus($transaction, Transaction::WAIT_FOR_CLIENT_VERIFICATION_STATUS);
            $call_back_url = 'monolitic.com/payment/callback/' . $transaction->factor_number;
            if ($call_back) {
                $data = [
                    'url' => $call_back_url,
                    'methode' => 'POST',
                    'data' =>
                        [
                            'gateway_token' => $transaction->gateway_token,
                            'factor_number' => $transaction->factor_number
                        ]
                ];
                return view('callback.form.client', $data);
            } else {
                return response()->json('transaction failed', 500);
            }


        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function verify(Request $request, Transaction $transaction)
    {

        try {
            $this->transactionService->validateTransactionStatus($transaction, Transaction::WAIT_FOR_CLIENT_VERIFICATION_STATUS);

            $gateway = GatewaySelector::getGateway($transaction);
            $verifyResponse = $gateway->verify($transaction);

            $this->transactionService->changeTransactionStatus($transaction, Transaction::VERIFIED_STATUS);

            if ($verifyResponse) {
                return response()->json(['transaction_status' => 'verified', 'status' => 1]);
            }

        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function getTransactionData(Request $request, Transaction $transaction)
    {
        try {
            $this->transactionService->validateTransactionStatus($transaction, Transaction::VERIFIED_STATUS);
            $gateway = GatewaySelector::getGateway($transaction);
            $data = $gateway->getTransactionData($transaction);

            $this->transactionService->changeTransactionStatus($transaction, Transaction::SUCCEED);
            if ($data != false) {
                return $data;
            }
            return false;

        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
