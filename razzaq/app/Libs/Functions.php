<?php

namespace App\Libs;

class Functions
{

    public function humanReadableNumber($number, $decimalPlaces = null, $language = 'fa')
    {
        // Check if the provided number is numeric
        if (!is_numeric($number)) {
            return "Invalid input. Please provide a valid number.";
        }
        // Convert the number to float to handle integer or float input
        $str_number = (string)$number;

        $is_neg = $str_number[0] === '-';
        $sign = $is_neg ? '-' : '';
        if ($is_neg) {
            $str_number = substr($str_number, 1);
        }
        // Validate the decimalPlaces argument
        if ($decimalPlaces !== null && !is_int($decimalPlaces)) {
            return "Invalid input for decimal places. Please provide a valid integer or null.";
        }


        if (stripos($number, 'e') === false && stripos($number, 'E') === false) {
            $exploded_number = explode(".", $str_number);
            $int_part = $exploded_number[0];
            $decimal_part = count($exploded_number) == 2 ? $exploded_number[1] : null;
        } else {

            $Scientific = $this->convertScientificToStr($str_number);
            $str_number = $Scientific[0];

            $decimal = $Scientific[1];


            $str_number = number_format($str_number, $decimal, '.', '');

            $exploded_number = explode(".", $str_number);
            $int_part = $exploded_number[0];
            $decimal_part = count($exploded_number) == 2 ? $exploded_number[1] : null;

        }


        $int_str = number_format($int_part);
        if ($decimal_part && $decimalPlaces !== null) {
            $decimal_str = substr($decimal_part, 0, $decimalPlaces);

            if (strlen($decimal_part) > $decimalPlaces) {
                $dec = '0.' . $decimal_part;
                $dec = round($dec, $decimalPlaces);
                if (stripos($dec, 'E') != false) {
                    $sci_dec = $this->convertScientificToStr($dec);
                    $dec = $sci_dec[0];

                }

                $decimal_str = substr($dec, 2, $decimalPlaces);
            }


        } else {
            $decimal_str = $decimal_part;
        }
        if ($decimal_str) {
            while (true) {
                $len = strlen($decimal_str);
                if ($len > 0 && $decimal_str[$len - 1] === '0') {
                    $decimal_str = substr($decimal_str, 0, $len - 1);
                } else {
                    break;
                }
            }
        }

        $formatted_number = $sign . $int_str;
        if ($decimal_str) {
            $formatted_number .= '.' . $decimal_str;
        }
        if ($formatted_number == '-0') {
            $formatted_number = '0';
        }

        if ($language === 'fa') {
            $formatted_number = $this->converter($formatted_number);
        }

        return $formatted_number;

    }


    public function convertScientificToStr($number)
    {

        $number = (string)$number;
        $decimal = 20;
        $decimalPart = explode('.', $number);
        if (count($decimalPart) > 1) {
            $decimalPartExplode = explode('E', $number);
            if (count($decimalPartExplode) == 1) {
                $decimalPartExplode = explode('e', $number);
            }
            if ($decimalPartExplode[1] < 0) {
                $decimal = $decimalPartExplode[1] * -1;
            } else {
                $decimal = $decimalPartExplode[1];
            }

            $b = explode('.', $decimalPartExplode[0]);
            $str_number_part = $b[0] . $b[1];

            $zero_part = '';
            for ($i = 1; $i < $decimal; $i++) {
                $zero_part .= '0';
            }
            if ($decimalPartExplode[1] < 0) {
                $number = '0.' . $zero_part . $str_number_part;
                $decimal = $decimal + strlen($str_number_part);
            } else {
                $number = $str_number_part . $zero_part;
                echo strlen($number) - 1;
                $number = substr($number, 0, strlen($number) - 1);
                $decimal = $decimal + strlen($str_number_part);
            }
        }
        return [$number, $decimal];
    }


    public function converter($number)
    {
        $persianDigits = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

        $res = str_replace(range(0, 9), $persianDigits, $number);
        $res = str_replace(',', '٬', $res);
        $res = str_replace('.', '٫', $res);
        return $res;
    }

}
