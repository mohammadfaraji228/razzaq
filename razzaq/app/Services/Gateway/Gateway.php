<?php


namespace App\Services\Gateway;

use App\Interfaces\Repositories\TransactionRepository;
use App\Models\Transaction;
use Illuminate\Contracts\Validation\Validator;
use mysql_xdevapi\Exception;

class Gateway
{
    protected $transaction;
    protected $transactionService;


    public function __construct(Transaction $transaction)
    {
        $this->transactionService = resolve(TransactionRepository::class);
        $this->transaction = $transaction;

    }

    protected function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    protected function getReturnUrl()
    {
        return sprintf(
            'bp.faraji.com/api/callback/transactions/%s',
            $this->transaction->token
        );
    }

    /**
     * @param Validator $validator
     */
    protected function validateResponseOrFail(Validator $validator)
    {
        if ($validator->fails()) {
            $errors = implode(',', $validator->getMessageBag()->all());
            throw new Exception();
        }
    }

    protected function saveCardNumber($cardNumberMask)
    {
        $this->transactionService->saveCardNumber($this->transaction, $cardNumberMask);
    }

    protected function saveExtraData(Transaction $transaction, array $data)
    {
        $oldData = (array)$transaction->extra_data;
        $finalData = array_merge($oldData, $data);
        $transaction->extra_data = $finalData;
        $transaction->save();
    }

    /**
     * @param Transaction $transaction
     * @param $refNum
     */
    protected function checkDoubleSpending(Transaction $transaction, $refNum)
    {
        if ($transaction->bank_txref == $refNum) {
            throw new Exception();
        }
    }

    protected function saveBankTxref($txref)
    {
        $this->transactionService->saveBankTxref($this->transaction, $txref);
    }

    /**
     * @param Transaction $transaction
     */
    public function getTransactionData(Transaction $transaction)
    {
        return
            [
                'txref' => $transaction->bank_txref,
                'card_number' => $transaction->card_number_mask,
                'factor_number' => $transaction->factor_number
            ];
    }

    public function saveBankToken($token): Transaction
    {
        return $this->transactionService->saveBankToken($this->transaction, $token);
    }

}
