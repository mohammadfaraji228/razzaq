<?php

namespace App\Services\Gateway;

use App\Models\Transaction;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Interfaces\Gateways\GatewayInterface;

class JibitGateway extends Gateway implements GatewayInterface
{

    const JIBIT_GATEWAY_ID = 1;

    /**
     * @inheritDoc
     */
    public function pay(Transaction $transaction)
    {
        $token = $this->getToken($transaction);
        $url = $transaction->gateway->data->payment_endpoint;
        $header = ['Authorization' => 'Bearer ' . $token];
        $data = [
            'currency' => 'IRR',
            'amount' => $transaction->amount,
            'callbackUrl' => $this->getReturnUrl(),
            'clientReferenceNumber' => $transaction->factor_number,
        ];
        $response = Http::withHeaders($header)->post($url, $data)->json();

        $paymentValidation = Validator::make($response, [
            'purchaseId' => 'required',
            'clientReferenceNumber' => 'required',
            'pspSwitchingUrl' => 'required',
        ]);

        $this->validateResponseOrFail($paymentValidation);
        $this->saveBankTxref($response['purchaseId']);
        $this->setGatewayId($transaction);
        return [
            'url' => $response['pspSwitchingUrl'],
            'methode' => 'GET',
            'data' => []
        ];
    }

    /**
     * @inheritDoc
     */
    public function callback(Transaction $transaction, Request $request)
    {
        $this->checkVerifyCallback($request->all());
        $this->saveCardNumber($request->payerMaskedCardNumber);
        $this->saveExtraData($transaction, [
            'pspReferenceNumber' => $request->pspReferenceNumber,
            'purchaseId' => $request->purchaseId,
            'clientReferenceNumber' => $request->clientReferenceNumber,
        ]);
        return true;
    }

    private function checkVerifyCallback(array $data)
    {
        if (empty($data['status']) || $data['status'] != 'SUCCESSFUL') {
            throw new Exception();
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function verify(Transaction $transaction)
    {
        $bankTxRef = $transaction->gateway_tx_ref;
        $urlVerify = $transaction->gateway->data->verify_transaction_url;
        $generateUrl = str_replace('{purchaseId}', $bankTxRef, $urlVerify);

        $token = $transaction->extra_data->accessToken ?? '';
        $verifyResponse = Http::withHeaders(['Authorization' => 'Bearer ' . $token])->post($generateUrl)->json();


        $this->checkVerifyStep($verifyResponse);

        return true;
    }

    private function checkVerifyStep(array $data): bool
    {
        if (empty($data['status']) || $data['status'] != 'SUCCESSFUL') {
            throw new Exception();
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getTransactionData(Transaction $transaction)
    {
        $data = [
            'apiKey' => $transaction->gateway->data->api_key,
            'secretKey' => $transaction->gateway->data->secret_key,
        ];

        $tokenResponse = Http::post($transaction->gateway->data->get_token_endpoint, $data)->json();
        $checkingGetJwtToken = Validator::make($tokenResponse, [
            'accessToken' => 'required',
            'refreshToken' => 'required'
        ]);
        $this->saveExtraData($transaction, $tokenResponse);
        $this->validateResponseOrFail($checkingGetJwtToken);
        if ($tokenResponse->purchaseId == $transaction->gateway_tx_ref) {
            $result = [
                'mask_card' => $tokenResponse->mask_card,
                'amount' => $tokenResponse->amount,
                'factor_number' => $transaction->factor_number,
            ];
            return $result;
        }
        return false;
    }

    private function getToken(Transaction $transaction)
    {
        $url = $transaction->gateway->data->get_token_endpoint;
        $data = [
            'apiKey' => $transaction->gateway->data->api_key,
            'secretKey' => $transaction->gateway->data->secret_key,
        ];

        $response = Http::post($url, $data)->json();

        $validate = Validator::make($response, [
            'accessToken' => 'required',
            'refreshToken' => 'required',
        ]);
        $this->validateResponseOrFail($validate);

        return $response['accessToken'];
    }

    private function setGatewayId(Transaction $transaction)
    {
        $transaction->gateway_id = static::JIBIT_GATEWAY_ID;
        $transaction->save();

        return $transaction;
    }
}
