<?php

namespace App\Services;

use App\Interfaces\Repositories\TransactionRepository;
use App\Models\Transaction;

class TransactionService implements TransactionRepository
{

    /**
     * @inheritDoc
     */

    public function createTransaction($data): array
    {
        $token = $this->generateToken();

        $factor_number = $data['factor_number'];

        $transaction = new Transaction();
        $transaction->user_id = $data['user_id'];
        $transaction->amount = $data['amount'];
        $transaction->factor_number = $factor_number;
        $transaction->token_expires_in = now()->addSeconds(config('transaction.token_expiration_interval_seconds'));
        $transaction->token = $token;
        $transaction->save();

        return ['token' => $token, 'factor_number' => $factor_number];
    }

    /**
     * @return string
     */
    private function generateToken(): string
    {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }

    public function findTransactionByToken($token): string
    {
        $transaction = Transaction::where('token', $token)->first();
        if ($transaction->token_expires_in > now()) {
            return $transaction;
        } else return false;
    }

    public function validateTransactionStatus($transaction, $status)
    {
        if ($transaction->status != $status) {
            return false;
        }
    }

    public function changeTransactionStatus($transaction, $status)
    {
        $transaction->status = $status;
        $transaction->save();
        return $transaction;
    }
}
