<?php

use App\Http\Controllers\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('bp')->group(function () {
    Route::prefix('requests')->group(function () {
        Route::post('payment-gateway', [PaymentController::class, 'getToken'])->name('getTokenRequest');
        Route::post('verify/{transaction:token}', [PaymentController::class, 'verify'])->name('verifyTransaction');
        Route::post('transaction/{transaction:token}', [PaymentController::class, 'getTransactionData'])->name('getTransactionData');
    });
});

Route::any('/callback/transactions/{transaction:token}', [PaymentController::class, 'callback'])->name('callback');
