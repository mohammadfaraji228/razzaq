<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('gateway_id');
            $table->string('token')->index()->unique();
            $table->dateTime('token_expires_in')->nullable();
            $table->decimal('amount', 30, 5);
            $table->enum('status', ['CREATED', 'EXPIRED', 'WAIT_FOR_CLIENT_VERIFICATION', 'SENT_BANK', 'VERIFIED', 'RETURNED_FROM_BANK'])->default('CREATED');
            $table->string('gateway_token')->nullable();
            $table->string('factor_number')->index();
            $table->string('card_number_mask')->nullable();
            $table->string('gateway_tx_ref')->nullable();
            $table->json('extra_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaction');
    }
};
