<?php

namespace Database\Seeders;

use App\Models\gateway;
use Illuminate\Database\Seeder;

class GatewaySeeder extends Seeder
{
    public function run()
    {
        $gateways = [
            [
                'id' => 1,
                'name' => 'jibit',
                'data' => [
                    'api_key' => 'dsafg',
                    'secret_key' => 'afgfsdbgkjvlkcnsdjfjkds',
                    'get_token_endpoint' => 'https://napi.jibit.ir/ppg/v3/tokens',
                    'payment_endpoint' => 'https://napi.jibit.ir/ppg/v3/purchases',
                    'verify_transaction_url' => 'https://napi.jibit.ir/ppg/v3/purchases/{purchaseId}/verify',
                ]
            ],
        ];

        foreach ($gateways as $gateway) {
            Gateway::firstOrCreate(['id' => $gateway['id']], $gateway);
        }
    }

}
