<form id="goto_gateway" class="form-horizontal" method="{{$method}}" action="{!! $url !!}">
    @yield('form-data')
    @foreach($data as $key => $value)
        <input type="hidden" name="{{$key}}" value="{{$value}}">
    @endforeach
    <noscript>
        <button type="submit" class="btn btn-success">کلیک کنید</button>
    </noscript>
</form>
<script type="text/javascript">
    var f = document.getElementById('goto_gateway');
    f.submit();
</script>
